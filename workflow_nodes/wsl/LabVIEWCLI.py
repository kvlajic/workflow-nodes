#!/usr/bin/python3
# This program wraps the property API around LabVIEWCLI to allow using it as a node in
# the workflow editor.
# Author: Institute for Applied Materials - Computational Materials Science (IAM-CMS),
#         Karlsruher Institut für Technologie (KIT)
# Date: 10.06.2020
# This software is Public Domain.
import subprocess
import sys

from xmlhelpy import command
from xmlhelpy import Integer
from xmlhelpy import option


@command(
    name="LabVIEWCLI",
    version="0.1.0",
    description="Wrapper node for LabVIEWCLI on MS Windows",
)
@option(
    "operation-name",
    char="o",
    description="Name of LabVIEWCLI operation (eg: MassCompile, CloseLabVIEW, RunVI,"
    " ...)",
    required=False,
)
@option(
    "port-number",
    char="n",
    description="Port on which the remote LabVIEW application is listening",
    param_type=Integer,
    required=False,
)
@option(
    "labview-path",
    char="p",
    description="Path of the LabVIEW in which the operation will run",
    required=False,
)
@option(
    "logfile-path",
    char="l",
    description="Path of the LabVIEWCLI log file.",
    required=False,
)
@option(
    "no-log-to-console",
    char="c",
    description="If set, the output gets logged only to log file (otherwise log"
    " file and console)",
    is_flag=True,
    required=False,
)
@option(
    "verbosity",
    char="v",
    description="This command line argument is used to control the output being logged."
    ' Default is "Default". Possible values are Minimal, Default, Detailed,'
    " Diagnostic",
    default="Default",
    required=False,
)
@option(
    "additional-operation-directory",
    char="a",
    description="Additional directory where LabVIEWCLI will look for additional"
    " operation class other than default location",
    required=False,
)
def execute(
    operation_name,
    port_number,
    labview_path,
    logfile_path,
    no_log_to_console,
    verbosity,
    additional_operation_directory,
):
    """LabVIEWCLI"""

    cmd = ["LabVIEWCLI.exe"]
    if operation_name:
        cmd += ["-OperationName", operation_name]
    if port_number:
        cmd += ["-PortNumber", str(port_number)]
    if labview_path:
        cmd += ["-LabVIEWPath", labview_path]
    if logfile_path:
        cmd += ["-LogFilePath", logfile_path]
    if no_log_to_console:
        cmd += ["-LogToConsole", "False"]
    if verbosity:
        cmd += ["-Verbosity", verbosity]
    if additional_operation_directory:
        cmd += ["-AdditionalOperationalDirectory"]
    sys.exit(subprocess.call(cmd))


if __name__ == "__main__":
    execute()
