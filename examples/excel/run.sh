#!/bin/bash

current_dir=$(dirname $(readlink -f $0))

# Excel file to read
file="${current_dir}/files/test.xlsx"

# Define start and end column of records
colums_start=E
colums_end=F

# Upload all data for one record
Excel2kadi $file -S $colums_start -E $colums_end -b "${current_dir}/files/" -f
