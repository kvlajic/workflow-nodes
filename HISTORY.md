# Release history

## 0.1.0 (2020-10-16)

* The first few tools for various common use-cases are provided, including
  tools for converting, plotting and analysing data, for generating reports and
  for connecting to a Kadi4Mat instance.
