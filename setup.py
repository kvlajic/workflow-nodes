# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

from setuptools import find_packages
from setuptools import setup


with open(os.path.join("workflow_nodes", "version.py")) as f:
    exec(f.read())


with open("README.md") as f:
    long_description = f.read()


setup(
    name="workflow-nodes",
    version=__version__,
    license="Apache-2.0",
    author="Karlsruhe Institute of Technology",
    description="Collection of nodes for use in workflows.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/iam-cms/workflows/workflow-nodes",
    packages=find_packages(),
    include_package_data=True,
    python_requires=">=3.6",
    zip_safe=False,
    classifiers=[],
    install_requires=[
        "click",
        "kadi-apy>=0.3.3",
        "matplotlib",
        "openpyxl",
        "pandas",
        "pylatex",
        "xmlhelpy>=0.1.0",
        "graphviz",
    ],
    extras_require={
        "dev": [
            "black==20.8b1",
            "pre-commit>=2.7.0",
            "pylint>=2.4.4,<2.5.0",
            "tox>=3.15.0",
        ]
    },
    # Please sort alphabetically via 'workflow_nodes.AAA.BBB:execute'
    entry_points={
        "console_scripts": [
            # converter
            "Excel2kadi = workflow_nodes.converter.Excel2kadi:execute",
            "FileConverter = workflow_nodes.converter.FileConverter:execute",
            "Kadi2kadi = workflow_nodes.converter.Kadi2kadi:execute",
            "Merge_tables = workflow_nodes.converter.Merge_tables:execute",
            "Pace2kadi = workflow_nodes.converter.Pace2kadi:execute",
            # debug
            "DebugPrint = workflow_nodes.debug.DebugPrint:execute",
            # excel
            "ExcelAddData = workflow_nodes.excel.ExcelAddData:execute",
            "ExcelData = workflow_nodes.excel.ExcelData:execute",
            "ExcelReadData = workflow_nodes.excel.ExcelReadData:execute",
            # misc
            "CreateSymlink = workflow_nodes.misc.CreateSymlink:execute",
            "ImageJMacro = workflow_nodes.misc.ImageJMacro:execute",
            "ImageJVariable = workflow_nodes.misc.ImageJVariable:execute",
            "RunScript = workflow_nodes.misc.RunScript:execute",
            # plot
            "PlotMatplotlib = workflow_nodes.plot.PlotMatplotlib:execute",
            "PlotVeusz = workflow_nodes.plot.PlotVeusz:execute",
            # repo
            "RecordVisualize = workflow_nodes.repo.RecordVisualize:execute",
            "RecordVisualizeAll = workflow_nodes.repo.RecordVisualizeAll:execute",
            # report
            "AttachmentReport = workflow_nodes.report.AttachmentReport:execute",
            "CompileLatexReport = workflow_nodes.report.CompileLatexReport:execute",
            "EndReport = workflow_nodes.report.EndReport:execute",
            "ImageReport = workflow_nodes.report.ImageReport:execute",
            "InputReport = workflow_nodes.report.InputReport:execute",
            "MathReport = workflow_nodes.report.MathReport:execute",
            "PlotReport = workflow_nodes.report.PlotReport:execute",
            "StartReport = workflow_nodes.report.StartReport:execute",
            "TextReport = workflow_nodes.report.TextReport:execute",
            # simulation
            "Simulation_mpipace3D = workflow_nodes.simulation.mpipace3D:execute",
            # system
            "system_awk = workflow_nodes.system.awk:execute",
            "system_bc = workflow_nodes.system.bc:execute",
            "system_cp = workflow_nodes.system.cp:execute",
            "system_echo = workflow_nodes.system.echo:execute",
            "system_mkdir = workflow_nodes.system.mkdir:execute",
            "system_paste = workflow_nodes.system.paste:execute",
            "system_sed = workflow_nodes.system.sed:execute",
            "system_sort = workflow_nodes.system.sort:execute",
            # wsl
            "LabVIEWCLI = workflow_nodes.wsl.LabVIEWCLI:execute",
            "WinShellRun = workflow_nodes.wsl.WinShellRun:execute",
        ]
    },
)
